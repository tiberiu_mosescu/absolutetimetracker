package com.myprojects.tib.abstimetracker.asynctaskUtils;

/**
 * Created by Tib on 08-May-15.
 */
public class SynchronizeTalker {

    public void doWait (long l) {
        synchronized (this) {
            try {
                this.wait(l);
            } catch (InterruptedException e) {

            }
        }
    }

    public void doNotify () {
        synchronized (this) {
            this.notifyAll();
        }
    }

    public void doWait() {
        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {

            }
        }
    }
}

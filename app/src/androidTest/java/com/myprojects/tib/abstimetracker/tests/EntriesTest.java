package com.myprojects.tib.abstimetracker.tests;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.activities.ViewEntries;
import com.myprojects.tib.abstimetracker.objects.ListViewRow;
import com.myprojects.tib.abstimetracker.objects.User;

import java.util.ArrayList;

/**
 * Created by Tib on 11-May-15.
 */
public class EntriesTest extends ActivityInstrumentationTestCase2<ViewEntries> {

    public EntriesTest() {
        super(ViewEntries.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        User user = User.getUserInstance();
        user.setAllRowsList(new ArrayList<ListViewRow>());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testEntries () throws Throwable {
        Activity activity = getActivity();
        assertNotNull("Activity is null", activity);
        Button weekEndingButton = (Button) activity.findViewById(R.id.viewEntries_listHeaderWeekBtn);
        ListView list = (ListView) activity.findViewById(R.id.myList);
        TextView emptyListTextView = (TextView) activity.findViewById(android.R.id.empty);

        assertNotNull(weekEndingButton);
        assertNotNull(list);
        assertNotNull(emptyListTextView);
    }
}

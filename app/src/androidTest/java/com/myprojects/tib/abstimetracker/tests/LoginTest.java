package com.myprojects.tib.abstimetracker.tests;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ProgressBar;

import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.activities.LoginActivity;
import com.myprojects.tib.abstimetracker.asynctaskUtils.SynchronizeTalker;
import com.myprojects.tib.abstimetracker.asynctaskUtils.TestTaskInterface;
import com.myprojects.tib.abstimetracker.changedAsyncTasks.LoginTask;
import com.myprojects.tib.abstimetracker.objects.User;
import com.myprojects.tib.abstimetracker.utils.Connection;

public class LoginTest extends ActivityInstrumentationTestCase2 <LoginActivity> implements TestTaskInterface{


    private SynchronizeTalker async = null;
    User user;
    public LoginTest() {
        super(LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        getActivity().finish();
    }


    public void testLogin () throws Throwable {
        final Activity activity = getActivity();

        user = User.getUserInstance();
        async = new SynchronizeTalker();
        final ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.login_progress);
        assertNotNull("Progress bar is null", progressBar);
        assertNull("I did not expect this", user.getAllRowsList());
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                LoginTask task = new LoginTask(activity, progressBar, LoginTest.this);
                task.execute("tiberiu.mosescu", "a8$t!b!");
            }
        });
        async.doWait();  // wait until async.doNotify() is called

    }

    @Override
    public void onDone() {
        //Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(ViewEntries.class.getName(), null, false);
        //Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5);
        assertNotNull("All rows list is null", user.getAllRowsList());
        assertEquals("No connection", true, Connection.isNetworkConnAvailable(getInstrumentation().getContext()));
        assertEquals("All rows list is empty", true, user.getAllRowsList().size() > 0);

        //next activity is opened
        //assertNotNull(nextActivity);
        async.doNotify();
    }
}

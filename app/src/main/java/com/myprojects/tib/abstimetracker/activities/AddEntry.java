package com.myprojects.tib.abstimetracker.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.myprojects.tib.abstimetracker.asyncTasks.AddEntryTask;
import com.myprojects.tib.abstimetracker.asyncTasks.EditEntryTask;
import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.adapters.SearchAdapter;
import com.myprojects.tib.abstimetracker.objects.EditUtilObject;
import com.myprojects.tib.abstimetracker.objects.ListViewRow;
import com.myprojects.tib.abstimetracker.objects.Project;
import com.myprojects.tib.abstimetracker.objects.User;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.DateConverter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Activity used with dual purpose : both add entry and edit entry.
 * Gets the date and time from the last entry for the current day (if it exists) from View Entries and sets the start TimePicker to that time.
 * Sets the date and end time to the current system time.
 * Watches description text so it isn't empty and after an entry is successfully added, sends a signal to View Entries to refresh by setting the refreshEntries flag.
 */
public class AddEntry extends Activity{
    //Singleton
    User user;
    //Identification
    private static final int START_DATE_DIALOG_ID = 996;
    private static final int START_TIME_DIALOG_ID = 998;
    private static final int END_TIME_DIALOG_ID = 999;
    //Dating
    private Calendar calStart;
    private Calendar calEnd;
    private int year;
    private int month;
    private int day;
    private Button startDate;
    //Timing
    private static final int MILLIS2HOURS = 1000*60*60;
    private Button startTime;
    private Button endTime;
    private Button saveButton;
    private int hour;
    private int minute;
    // Sharing preferences in private
    private SharedPreferences prefs;
    //Wooing
    private EditText description;
    //Cashing in
    MenuItem saveIcon;
    //Adapting to circumstances
    private SearchAdapter searchAdapter;
    private ArrayList<String> categories = new ArrayList<>();
    private ArrayAdapter<String> adapterCategory;
    //Last time we talked
    String selectedProjectId;
    String selectedCategoryId;
    //private Spinner categorySpinner;
    //Object of desire
    private Project selectedProject;
    //Exchanged fluids (passed object)
    private ListViewRow object;
    private boolean isEditWindow;
    private String lastEntryDate;
    private String lastEntryTime;

    //Animate me
    LinearLayout LLparent;

    //Search your soul
    LinearLayout LLSearch;
    ListView searchListView;

    private ArrayList<Project> projectsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entry_new);
        user = User.getUserInstance();
        projectsList = user.getProjectArrayList();
        initializeFields();
        Intent intent = getIntent();
        isEditWindow = intent.getBooleanExtra(Details.EXTRA_IS4EDITING, false);
        if (user.getAllRowsList() != null && user.getAllRowsList().size()>0) {
            lastEntryDate = user.getAllRowsList().get(user.getAllRowsList().size() - 1).getEntryDate();
            lastEntryTime = user.getAllRowsList().get(user.getAllRowsList().size() - 1).getEndHour();
        }
        if (isEditWindow) {
            setTitle("EDIT");
            object = (ListViewRow) intent.getSerializableExtra(ViewEntries.EXTRA_PASSED_ENTRY);
            if (object != null) {
                setupFieldsForEditing(object);
                setSpinnerContent(object);
            }
        }
        else  {
            setTitle("ADD ENTRY");
            setCurrentDateOnView();
            setSpinnerContent(object);
        }

    }
    public void initializeFields () {
        searchAdapter = new SearchAdapter(this, user.getAllRowsList());
        searchListView = (ListView) findViewById(R.id.add_entry_searchResultLV);
        TextView emptyListTextView = (TextView) findViewById(android.R.id.empty);
        searchListView.setAdapter(searchAdapter);
        searchListView.setEmptyView(emptyListTextView);
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(AddEntry.this, Details.class);
                ListViewRow object2pass = searchAdapter.getAdapterList().get(position);
                intent.putExtra(ViewEntries.EXTRA_PASSED_ENTRY, object2pass);
                startActivity(intent);
                finish();
            }
        });
        LLSearch = (LinearLayout) findViewById(R.id.add_entry_searchLL);
        LLparent = (LinearLayout) findViewById(R.id.add_entry_layout);
        startDate = (Button) findViewById(R.id.add_entry_startDate);
        startTime = (Button) findViewById(R.id.add_entry_startTime);
        endTime = (Button) findViewById(R.id.add_entry_finishTime);
        endTime.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                resetFinishTime(v);
                return true;
            }
        });
        saveButton = (Button) findViewById(R.id.add_entry_btnSaveEntry);
        saveButton.setEnabled(true); // gets disabled after save
        description = (EditText) findViewById(R.id.add_entry_description);
        description.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i==KeyEvent.KEYCODE_ENTER) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (getCurrentFocus() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (saveIcon != null && description.getText().length()>0) {
                    saveIcon.setIcon(R.drawable.ic_action_content_save);
                    saveButton.setEnabled(true);
                }
                else {
                    if (saveIcon != null)
                    saveIcon.setIcon(R.drawable.ic_action_content_save_disable);
                    saveButton.setEnabled(false);
                }
            }
        });

        calStart = Calendar.getInstance();
        calEnd = Calendar.getInstance();
        year = calStart.get(Calendar.YEAR);
        month = calStart.get(Calendar.MONTH);
        day = calStart.get(Calendar.DAY_OF_MONTH);
        hour = calStart.get(Calendar.HOUR);
        minute = calStart.get(Calendar.MINUTE);
        prefs = getSharedPreferences(User.PREFS_NAME, MODE_PRIVATE);
    }

    private void setupFieldsForEditing(ListViewRow object) {
        String startDateString = object.getEntryDate() + " " +object.getStartHour();
        String endDateString = object.getEntryDate() + " " + object.getEndHour();
        calStart.setTime(DateConverter.convertStringToDate(startDateString, getApplicationContext()));
        calEnd.setTime(DateConverter.convertStringToDate(endDateString, getApplicationContext()));
        startDate.setText(object.getEntryDate());
        startTime.setText(object.getStartHour());
        endTime.setText(object.getEndHour());
        description.setText(object.getDescription());


    }
    @SuppressLint("SimpleDateFormat")
    private void setCurrentDateOnView() {

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy");
        SimpleDateFormat stf = new SimpleDateFormat("hh:mm a");
        String currentDate = sdf.format(new Date());
        String currentTime = stf.format(new Date());
        startDate.setText(currentDate);
        if (lastEntryTime != null && currentDate.equals(lastEntryDate)) {
            startTime.setText(lastEntryTime);
            calStart.setTime(DateConverter.convertStringToDate(lastEntryDate + " " + lastEntryTime, getApplicationContext()));
        }
        else
        startTime.setText(currentTime);
        endTime.setText(currentTime);
    }


    private void setSpinnerContent(ListViewRow object) {
        final ListViewRow obj = object;

        //Projects spinner
        Spinner projectSpinner = (Spinner) findViewById(R.id.add_entry_project);
        final Spinner categorySpinner = (Spinner) findViewById(R.id.add_entry_category);

        ArrayList<String> projectNames = new ArrayList<>();
        for (Project project : projectsList) {
            projectNames.add(project.getName());
        }
        if (projectsList != null && projectsList.size()>0)
        categories = projectsList.get(0).getCategories();
        else {
            projectSpinner.setEnabled(false);
            categorySpinner.setEnabled(false);
        }
        ArrayAdapter<String> adapterProject = new ArrayAdapter<>(AddEntry.this, android.R.layout.simple_spinner_item, projectNames);
        projectSpinner.setAdapter(adapterProject);
        if (object != null) {
            projectSpinner.setSelection(adapterProject.getPosition(object.getProject()));
        }

        projectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               if (saveIcon != null)
                saveIcon.setIcon(R.drawable.ic_action_content_save);
                saveButton.setEnabled(true);
                selectedProject = projectsList.get(i);
                selectedProjectId = projectsList.get(i).getProjectId() + "";
                categories = projectsList.get(i).getCategories();
                adapterCategory = new ArrayAdapter<>(AddEntry.this, android.R.layout.simple_spinner_item, categories);
                categorySpinner.setAdapter(adapterCategory);
                if (obj != null && prefs.getString(obj.getCategory(), null) != null)
                    categorySpinner.setSelection(adapterCategory.getPosition(prefs.getString(obj.getCategory(), null)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if (projectsList != null && projectsList.size()>0)
                selectedProjectId = projectsList.get(0).getProjectId() + "";
            }
        });

        //Categories spinner

        adapterCategory = new ArrayAdapter<>(AddEntry.this, android.R.layout.simple_spinner_item, categories);
        categorySpinner.setAdapter(adapterCategory);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (saveIcon != null)
                    saveIcon.setIcon(R.drawable.ic_action_content_save);
                saveButton.setEnabled(true);
                selectedCategoryId = selectedProject.getCategoryId().get(i) + "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if (projectsList != null && projectsList.size() > 0) {
                    if (selectedProject.getCategoryId().size() > 0) {
                        selectedCategoryId = selectedProject.getCategoryId().get(0) + "";
                    } else selectedCategoryId = "";
                }
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_entry, menu);
        saveIcon = menu.findItem(R.id.action_add_saved);
        saveIcon.setEnabled(false);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_add_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView searchText = (TextView) searchView.findViewById(id);
        searchText.setTextColor(Color.YELLOW);
        searchText.setHintTextColor(Color.WHITE);
        //searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                LLSearch.setVisibility(View.INVISIBLE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                AddEntry.this.searchAdapter.getFilter().filter(s);
                if (s.length() > 2) {
                    LLSearch.setVisibility(View.VISIBLE);
                } else {
                    LLSearch.setVisibility(View.GONE);
                }
                return true;
            }
        });

        return true;
    }


    // onClick methods from XML
    public void setStartDate(View view) {
        saveIcon.setIcon(R.drawable.ic_action_content_save);
        saveButton.setEnabled(true);
        createdDialog(START_DATE_DIALOG_ID).show();
    }

    public void setStartTime(View view) {
        saveIcon.setIcon(R.drawable.ic_action_content_save);
        saveButton.setEnabled(true);
        createdDialog(START_TIME_DIALOG_ID).show();
    }

    public void setEndTime(View view) {
        saveIcon.setIcon(R.drawable.ic_action_content_save);
        saveButton.setEnabled(true);
        createdDialog(END_TIME_DIALOG_ID).show();
    }

    public void resetFinishTime(View view) {
        Calendar currentTime = Calendar.getInstance();
        String am_pm;
        hour = currentTime.get(Calendar.HOUR_OF_DAY);
        minute = currentTime.get(Calendar.MINUTE);
        calEnd.set(Calendar.HOUR_OF_DAY, currentTime.get(Calendar.HOUR_OF_DAY));
        calEnd.set(Calendar.MINUTE, currentTime.get(Calendar.MINUTE));
        if (calEnd.get(Calendar.AM_PM) == Calendar.AM) {
            am_pm = " AM";
        }
        else {
            am_pm = " PM";
        }
        if (hour == 0) {
            hour += 12;
        }
        if (hour>12) {
            hour -= 12;
        }
        String curTime = String.format("%02d:%02d", hour, minute);
        endTime.setText(new StringBuilder()
                .append(curTime)
                .append(am_pm));
    }

    // Method for the datepicker and 2 timepickers
    protected Dialog createdDialog (int id) {
        switch (id) {
            case START_DATE_DIALOG_ID:
                return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                        year = i;
                        month = i2;
                        day = i3;
                        calStart.set(year, month, day);
                        calEnd.set(year, month, day);
                        startDate.setText(new StringBuilder().
                                append(calStart.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))
                                .append(", ")
                                .append(day)
                                .append(" ").append(calStart.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US))
                                .append(" ").append(year));
                        saveIcon.setIcon(R.drawable.ic_action_content_save);
                    }
                }, calStart.get(Calendar.YEAR), calStart.get(Calendar.MONTH), calStart.get(Calendar.DAY_OF_MONTH));
            case START_TIME_DIALOG_ID:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i2) {
                        String am_pm;
                        hour = i;
                        minute = i2;
                        calStart.set(Calendar.HOUR_OF_DAY, hour);
                        calStart.set(Calendar.MINUTE, minute);
                        if (calStart.get(Calendar.AM_PM) == Calendar.AM) {
                            am_pm = " AM";
                        }
                        else {
                            am_pm = " PM";
                        }
                        if (hour == 0) {
                            hour += 12;
                        }
                        if (hour>12) {
                            hour -= 12;
                        }
                        String curTime = String.format("%02d:%02d", hour, minute);
                        startTime.setText(new StringBuilder()
                        .append(curTime)
                        .append(am_pm));
                        saveIcon.setIcon(R.drawable.ic_action_content_save);
                    }
                }, calStart.get(Calendar.HOUR_OF_DAY), calStart.get(Calendar.MINUTE), false);
            case END_TIME_DIALOG_ID:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i2) {
                        String am_pm;
                        hour = i;
                        minute = i2;
                        calEnd.set(Calendar.HOUR_OF_DAY, hour);
                        calEnd.set(Calendar.MINUTE, minute);
                        if (calEnd.get(Calendar.AM_PM) == Calendar.AM) {
                            am_pm = " AM";
                        }
                        else {
                            am_pm = " PM";
                        }
                        if (hour == 0) {
                            hour += 12;
                        }
                        if (hour>12) {
                            hour -= 12;
                        }
                        String curTime = String.format("%02d:%02d", hour, minute);
                        endTime.setText(new StringBuilder()
                                .append(curTime)
                                .append(am_pm));
                        saveIcon.setIcon(R.drawable.ic_action_content_save);
                    }
                }, calEnd.get(Calendar.HOUR_OF_DAY), calEnd.get(Calendar.MINUTE), false);
        }
        return null;
    }

    public void saveEntry(View view) {
        //push my save button
        if (Connection.isNetworkConnAvailable(this)) {
            DecimalFormat df = new DecimalFormat("#0.000");
            if (description.getText().toString().length()>0) {
                double durationT = (double) (calEnd.getTimeInMillis() - calStart.getTimeInMillis()) / MILLIS2HOURS;
                if (durationT >= 0.01) {
                    String entryId = null;
                    if (isEditWindow && object != null) {
                        entryId = object.getEntryLogId() + "";
                    }
                    String duration = df.format(durationT);
                    String descriptionText = description.getText().toString();
                    String categoryId = selectedCategoryId;
                    String endHour = endTime.getText().toString();
                    String entryDate = "/Date(" + DateConverter.convertToGmt(calStart) + ")/";
                    String issueId = "0";
                    String moduleId = "0";
                    String projectId = selectedProjectId;
                    String startHour = startTime.getText().toString();

                    EditUtilObject editUtilObject = new EditUtilObject(entryId, duration, descriptionText, categoryId, endHour, entryDate, issueId, moduleId, projectId, startHour);


                        saveButton.setEnabled(false);
                        if (isEditWindow) { //editing entry
                            EditEntryTask task = new EditEntryTask(AddEntry.this, new EditEntryTask.EditEntryListerer() {
                                @Override
                                public void onEditSuccessful() {
                                    Toast.makeText(AddEntry.this, "Edit successful", Toast.LENGTH_SHORT).show();
                                    saveIcon.setIcon(R.drawable.ic_action_content_save_disable);
                                    ViewEntries.setRefreshEntries(true);
                                }

                                @Override
                                public void onEditFail() {
                                    Toast.makeText(AddEntry.this, "You logged in somewhere else, re-login necessary", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(AddEntry.this, SplashScreenActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            task.execute(editUtilObject);
                        } else {
                            AddEntryTask task = new AddEntryTask(AddEntry.this, new AddEntryTask.AddEntryListener() {
                                @Override
                                public void onAddEntrySuccess() {
                                    LLparent.setVisibility(View.VISIBLE);
                                    Animation animation = AnimationUtils.loadAnimation(AddEntry.this, R.anim.fade_in);
                                    animation.setDuration(500);
                                    LLparent.startAnimation(animation);
                                    saveIcon.setIcon(R.drawable.ic_action_content_save_disable);
                                    description.getText().clear();
                                    calStart = Calendar.getInstance();
                                    calEnd = Calendar.getInstance();
                                    lastEntryTime = endTime.getText().toString();
                                    lastEntryDate = startDate.getText().toString();
                                    setCurrentDateOnView();
                                    ViewEntries.setRefreshEntries(true);
                                }

                                @Override
                                public void onAddEntryFail() {
                                    Toast.makeText(AddEntry.this, "You logged in somewhere else, re-login necessary", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(AddEntry.this, SplashScreenActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            task.execute(editUtilObject);
                            Animation animation = AnimationUtils.loadAnimation(AddEntry.this, R.anim.push_left_out);
                            animation.setDuration(500);
                            animation.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    LLparent.setVisibility(View.INVISIBLE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            LLparent.startAnimation(animation);
                        }

                } else {
                    Toast.makeText(this, "Time is non-positive", Toast.LENGTH_SHORT).show();
                }
            }
                else {
                    Toast.makeText(this, "Please enter a description", Toast.LENGTH_SHORT).show();
                }
        }
        else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }


}

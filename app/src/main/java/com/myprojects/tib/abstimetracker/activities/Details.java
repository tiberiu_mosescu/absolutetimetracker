package com.myprojects.tib.abstimetracker.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.myprojects.tib.abstimetracker.asyncTasks.DeleteTask;
import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.objects.ListViewRow;

/**
 * When the user clicks an item in the View Entries ListView, it shows its full details by using this activity.
 * Has delete button which deletes the current entry and the edit button which starts the Add Entry activity (more details there).
 */
public class Details extends Activity {

    public static final String EXTRA_IS4EDITING = "isForEditing";
    MenuItem deleteMenuItem;
    ListViewRow object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent intent = getIntent();
        object = (ListViewRow) intent.getSerializableExtra(ViewEntries.EXTRA_PASSED_ENTRY);
        TextView description = (TextView) findViewById(R.id.details_description);
        description.setText(object.getDescription());
        TextView project = (TextView) findViewById(R.id.details_project);
        project.setText(object.getProject());
        TextView category = (TextView) findViewById(R.id.details_category);
        category.setText(object.getCategory());
        TextView startDate = (TextView) findViewById(R.id.details_startHour);
        startDate.setText(object.getEntryDate() + " " + object.getStartHour());
        TextView endDate = (TextView) findViewById(R.id.details_endHour);
        endDate.setText(object.getEntryDate() + " " + object.getEndHour());
        TextView duration = (TextView) findViewById(R.id.details_duration);
        duration.setText(object.getDuration());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.details, menu);
        deleteMenuItem = menu.findItem(R.id.detailMenu_delete);
        deleteMenuItem.setEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.detailMenu_edit:
                Intent intent = new Intent(Details.this, AddEntry.class);
                intent.putExtra(ViewEntries.EXTRA_PASSED_ENTRY, object);
                intent.putExtra(EXTRA_IS4EDITING, true);
                startActivity(intent);
                finish();
                return true;
            case R.id.detailMenu_delete:
                DeleteTask deleteTask = new DeleteTask(this, new DeleteTask.DeleteEntryListener() {
                    @Override
                    public void onDeleteSuccessful() {
                        ViewEntries.setRefreshEntries(true);
                        deleteMenuItem.setEnabled(false);
                        Toast.makeText(Details.this, "Delete successful", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void onDeleteFail() {
                        Toast.makeText(Details.this, "You logged in somewhere else, please login again", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Details.this, SplashScreenActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
                deleteTask.execute(object.getEntryLogId());
        }

        return super.onOptionsItemSelected(item);

    }

}

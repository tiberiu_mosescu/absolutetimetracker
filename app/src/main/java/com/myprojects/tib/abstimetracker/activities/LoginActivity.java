package com.myprojects.tib.abstimetracker.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.myprojects.tib.abstimetracker.asyncTasks.LoginTask;
import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.objects.User;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.Encryption;

import java.util.ArrayList;

/**
 * Simple login screen with an autocomplete username field, a password field and a login button.
 */
public class LoginActivity extends Activity {

    //Shared preferences
     private SharedPreferences prefs;

    //Views
    private AutoCompleteTextView emailField;
    private EditText passwordField;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Crashlytics.start(this);
        setContentView(R.layout.my_login_layout);
        prefs = getSharedPreferences(User.PREFS_NAME, MODE_PRIVATE);
        ArrayList<String> emailStuff = new ArrayList<>();
        String lastEmail = prefs.getString("emailAutoComplete", null);
        final String lastPassword = prefs.getString("passwordAutoComplete", null);
        emailField = (AutoCompleteTextView) findViewById(R.id.txt_email);
        if (lastEmail != null) {
            emailStuff.add(lastEmail);
            // Set autocomplete for e-mail
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_dropdown_item_1line, emailStuff);
            emailField.setAdapter(adapter);
            emailField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    passwordField.setText(lastPassword);
                }
            });
        }
        //Password
        passwordField = (EditText) findViewById(R.id.txt_password);
        //Sign in button
        Button btnSignIn = (Button) findViewById(R.id.btn_signIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connection.isNetworkConnAvailable(LoginActivity.this)) {
                    prefs.edit().putString("email", emailField.getText().toString())
                            .putString("password", passwordField.getText().toString())
                            .putString("emailAutoComplete", emailField.getText().toString())
                            .putString("passwordAutoComplete", passwordField.getText().toString())
                            .putString("password64", Encryption.toBase64(passwordField.getText().toString()))
                            .apply();
                    LoginTask task = new LoginTask(LoginActivity.this, progressBar);
                    User user = User.getUserInstance();
                    user.setUserName(emailField.getText().toString());
                    user.setPassword(passwordField.getText().toString());
                    task.execute(user.getUserName(), user.getPassword());
                }
                else {
                    Toast.makeText(LoginActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Progress bar
        progressBar = (ProgressBar) findViewById(R.id.login_progress);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        super.onBackPressed();
    }
}

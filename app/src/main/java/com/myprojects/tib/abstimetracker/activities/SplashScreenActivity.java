package com.myprojects.tib.abstimetracker.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.myprojects.tib.abstimetracker.asyncTasks.SplashLoginTask;
import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.objects.User;

/**
 * Very similar to login, except is does auto-login by getting the username and password from SharedPreferences.
 * If auto-login fails, it redirects the user to regular login.
 */
public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Crashlytics.start(this);
        setContentView(R.layout.activity_splash_screen);
        SharedPreferences prefs = getSharedPreferences(User.PREFS_NAME, MODE_PRIVATE);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.splash_progress);
        if (prefs.getString("email", null) != null && prefs.getString("password64", null) != null) {
            SplashLoginTask task = new SplashLoginTask(this, progressBar);
            task.execute();
        } else {
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}

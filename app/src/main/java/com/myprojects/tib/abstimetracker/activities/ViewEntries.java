package com.myprojects.tib.abstimetracker.activities;

import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.LineRegion;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.SeriesRenderer;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.TextOrientationType;
import com.androidplot.ui.XLayoutStyle;
import com.androidplot.ui.YLayoutStyle;
import com.androidplot.ui.widget.TextLabelWidget;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;
import com.myprojects.tib.abstimetracker.asyncTasks.RefreshEntriesTask;
import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.adapters.CustomAdapter;
import com.myprojects.tib.abstimetracker.dialogs.CaldroidDialog;
import com.myprojects.tib.abstimetracker.dialogs.LogoutDialog;
import com.myprojects.tib.abstimetracker.objects.ListViewRow;
import com.myprojects.tib.abstimetracker.objects.User;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.DateConverter;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Main screen after login. In landscape it shows the list, in portrait it shows the plot.
 * If communication with the server occurs as when an entry is added/deleted/edited, it is necessary to refresh (refetch) the entries for the ListView.
 * Uses the flag refreshEntries to signal a refresh.
 */
public class ViewEntries extends FragmentActivity {
    public static final String EXTRA_PASSED_ENTRY = "com.myprojects.tib.abstimetracker.activities.entry";
    //Singleton
    private User user;

    //Preferences and flags
    private SharedPreferences prefs;
    private static boolean refreshEntries;

    //Portrait views
    private Button weekEndingButton;
    private ListView list;
    private CustomAdapter customAdapter;

    //Landscape views
    private XYPlot plot;

    //Plot variables
    private static final String NO_SELECTION_TEXT = "Touch bar to view hours";
    private static final String[] X_LABELS = {"", "Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", ""};
    private static final String[] DAYS = {"Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri"};
    private Double[] plotYHours = {0.0};
    private Pair<Integer, XYSeries> selection;
    private MyBarFormatter noSelectionFormatter;
    private MyBarFormatter selectionFormatter;
    private TextLabelWidget dayHoursLabel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Globals
        user = User.getUserInstance();
        prefs = getSharedPreferences(User.PREFS_NAME, MODE_PRIVATE);
        customAdapter = new CustomAdapter(this, user.getAllRowsList());
        //Portrait
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_view_entries);
            initializePortraitViews();
        }
        //Landscape
        else {
            ActionBar bar = getActionBar();
            if (bar != null) {
                bar.hide();
            }
            setContentView(R.layout.activity_view_entries);
            initializeLandscapeViews();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_entries, menu);
                // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.viewMenu_search).getActionView();
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView searchTextView = (TextView) searchView.findViewById(id);
        searchTextView.setTextColor(Color.YELLOW);
        searchTextView.setHintTextColor(Color.WHITE);
        searchTextView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null)
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ViewEntries.this.customAdapter.getFilter().filter(s);
                return true;
            }
        });
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.viewMenu_add:
                Intent intent = new Intent(this, AddEntry.class);
                startActivity(intent);
                break;
            case R.id.viewMenu_logout:
                LogoutDialog.createDialog(this, prefs);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Connection.isNetworkConnAvailable(this)) {
            if (refreshEntries) {
                RefreshEntriesTask task = new RefreshEntriesTask(this, prefs, new RefreshEntriesTask.RefreshEntriesListener() {
                    @Override
                    public void onPreRefreshEntries() {
                        if (list != null)
                            list.setVisibility(View.INVISIBLE);
                        else {
                            plot.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onEntriesRefreshed() {
                        customAdapter.clear();
                        customAdapter.addAll(user.getAllRowsList());
                        refreshEntries = false;
                        if (list != null) {
                            list.setVisibility(View.VISIBLE);
                            Animation animation = AnimationUtils.loadAnimation(ViewEntries.this, R.anim.fade_in);
                            animation.setDuration(500);
                            list.setAnimation(animation);
                        }
                        else {
                            updatePlot();
                            plot.setVisibility(View.VISIBLE);
                            Animation animation = AnimationUtils.loadAnimation(ViewEntries.this, R.anim.fade_in);
                            animation.setDuration(500);
                            plot.setAnimation(animation);
                        }
                    }

                    @Override
                    public void onEntryRefreshFail() {

                    }
                });
                task.execute(prefs.getInt(User.PREFS_WEEK_ENDING, 0));
            }
        }
        else {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    // My methods
    private void initializePortraitViews() {
        weekEndingButton = (Button) findViewById(R.id.viewEntries_listHeaderWeekBtn);
        list = (ListView) findViewById(R.id.myList);
        TextView emptyListTextView = (TextView) findViewById(android.R.id.empty);
        list.setAdapter(customAdapter);
        list.setEmptyView(emptyListTextView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ViewEntries.this, Details.class);
                ListViewRow object2pass = customAdapter.getAdapterList().get(position);
                intent.putExtra(EXTRA_PASSED_ENTRY, object2pass);
                startActivity(intent);
            }
        });
    }

    private void initializeLandscapeViews() {
        //range = Y axis, domain = X axis
        plot = (XYPlot) findViewById(R.id.mySimpleXYPlot);
        //set domain labels as string [x-axis]
        plot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
        plotYHours = getDataToPlot();
        double max = -1;
        for (double d: plotYHours) {
            if (d>max)
                max=d;
        }
        //y-axis
        plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
        plot.setRangeBoundaries(0, max + 2, BoundaryMode.FIXED);
        Paint paint = new Paint();
        paint.setColor(getResources().getColor(R.color.orange));
        //beyond axes
        plot.setBackgroundPaint(paint);
        //around axes
        plot.getGraphWidget().getBackgroundPaint().setColor(getResources().getColor(R.color.orange));
        //inside the plot
        plot.getGraphWidget().getGridBackgroundPaint().setColor(getResources().getColor(R.color.white));

        plot.setPlotMargins(0, 0, 0, 0);
        plot.getGraphWidget().setDomainValueFormat(new GraphXLabelFormat());
        //Format plot bars
        noSelectionFormatter = new MyBarFormatter(Color.argb(200, 100, 150, 100), Color.LTGRAY);
        selectionFormatter = new MyBarFormatter(getResources().getColor(R.color.slightOrange), Color.WHITE);
        dayHoursLabel = new TextLabelWidget(plot.getLayoutManager(), NO_SELECTION_TEXT,
                                                new SizeMetrics(
                                                        PixelUtils.dpToPix(100), SizeLayoutType.ABSOLUTE,
                                                        PixelUtils.dpToPix(100), SizeLayoutType.ABSOLUTE),
                                                        TextOrientationType.HORIZONTAL);
        dayHoursLabel.getLabelPaint().setTextSize(PixelUtils.dpToPix(16));
        // add a dark, semi-transparent background to the selection label widget:
        Paint p = new Paint();
        p.setARGB(100, 0, 0, 0);
        dayHoursLabel.setBackgroundPaint(p);
        dayHoursLabel.position(
                0.1f, XLayoutStyle.RELATIVE_TO_CENTER,
                PixelUtils.dpToPix(45), YLayoutStyle.ABSOLUTE_FROM_TOP,
                AnchorPosition.TOP_MIDDLE);
        dayHoursLabel.pack();
        //Add data to plot
        XYSeries initialValues = new SimpleXYSeries(Arrays.asList(plotYHours), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "");
        plot.addSeries(initialValues, noSelectionFormatter);
        //Set bar width
        MyBarRenderer renderer = ((MyBarRenderer) plot.getRenderer(MyBarRenderer.class));
        renderer.setBarWidth(50);
        //hide legend
        plot.getLegendWidget().setVisible(false);
        plot.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    onPlotClicked(new PointF(event.getX(), event.getY()));
                }
                return true;
            }
        });
    }
        private void onPlotClicked(PointF point) {

        // make sure the point lies within the graph area.  we use gridrect
        // because it accounts for margins and padding as well.
        if (plot.getGraphWidget().getGridRect().contains(point.x, point.y)) {
            Number x = plot.getXVal(point);
            Number y = plot.getYVal(point);


            selection = null;
            double xDistance = 0;
            double yDistance = 0;

            // find the closest value to the selection:
            for (XYSeries series : plot.getSeriesSet()) {
                for (int i = 0; i < series.size(); i++) {
                    Number thisX = series.getX(i);
                    Number thisY = series.getY(i);
                    if (thisX != null && thisY != null) {
                        double thisXDistance =
                                LineRegion.measure(x, thisX).doubleValue();
                        double thisYDistance =
                                LineRegion.measure(y, thisY).doubleValue();
                        if (selection == null) {
                            selection = new Pair<>(i, series);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        } else if (thisXDistance < xDistance) {
                            selection = new Pair<>(i, series);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        } else if (thisXDistance == xDistance &&
                                thisYDistance < yDistance &&
                                thisY.doubleValue() >= y.doubleValue()) {
                            selection = new Pair<>(i, series);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        }
                    }
                }
            }

        } else {
            // if the press was outside the graph area, deselect:
            selection = null;
        }

        if(selection == null) {
            dayHoursLabel.setText(NO_SELECTION_TEXT);
        } else {
            DecimalFormat df = new DecimalFormat("#0.00");
            if (!"none".equals(indexToDay((Integer)selection.second.getX(selection.first))))
            dayHoursLabel.setText("Day: " + indexToDay((Integer)selection.second.getX(selection.first)) +
                    "     Hours: " + df.format(selection.second.getY(selection.first)));
            else
                dayHoursLabel.setText(NO_SELECTION_TEXT);

        }
        plot.redraw();
    }
        public String indexToDay (int dayIndex) {
        String[] days = {"Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri"};
        if (dayIndex>0 && dayIndex<8)
            return days[dayIndex-1];
        else
            return "none";
    }

    /**
     * Returns the series to be plotted
     */
    private Double[] getDataToPlot() {
        User user = User.getUserInstance();
        ArrayList<ListViewRow> listForPlot;
        if (customAdapter.getAdapterList() != null)
            listForPlot = customAdapter.getAdapterList();
        else {
            listForPlot = user.getAllRowsList();
        }
        Double[] sums = new Double[9];
        for (int i = 0; i < 9; i++)
            sums[i] = 0.00;
        int counter = 1;
        String date4comparison = DAYS[0];

        for (ListViewRow object : listForPlot) {
            while (!object.getEntryDate().toLowerCase().contains(date4comparison.toLowerCase())) {
                counter++;
                date4comparison = DAYS[counter - 1];
            }
            sums[counter] += Double.parseDouble(object.getDuration());
        }
        return sums;
    }

    private void updatePlot() {
        //clear previous points
        for (XYSeries setElement : plot.getSeriesSet()) {
            plot.removeSeries(setElement);
        }
        plotYHours = getDataToPlot();
        double max = -1;
        for (double d: plotYHours) {
            if (d>max)
                max=d;
        }
        plot.setRangeBoundaries(0, max + 2, BoundaryMode.FIXED);
        XYSeries newValues = new SimpleXYSeries(Arrays.asList(plotYHours), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "");
        plot.addSeries(newValues, noSelectionFormatter);
        plot.redraw();
    }

    //XML onClick method
    public void openCaldroidDialog(final View view) {
        CaldroidDialog.showCaldroidDialog(this, prefs, new CaldroidDialog.CaldroidDateListener() {
            @Override
            public void onDateChosen(Date date, Calendar cal, int weekEndingId, View caldroidCell) {
                Animation selectedGoodDateAnimation = AnimationUtils.loadAnimation(ViewEntries.this, R.anim.wave_scale);
                selectedGoodDateAnimation.setDuration(500);
                caldroidCell.startAnimation(selectedGoodDateAnimation);
                RefreshEntriesTask daTask = new RefreshEntriesTask(ViewEntries.this, prefs, new RefreshEntriesTask.RefreshEntriesListener() {
                    @Override
                    public void onPreRefreshEntries() {
                        if (list != null)
                            list.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onEntriesRefreshed() {
                        customAdapter.clear();
                        customAdapter.addAll(user.getAllRowsList());
                        refreshEntries = false;
                        if (list != null) {
                            list.setVisibility(View.VISIBLE);
                            Animation animation = AnimationUtils.loadAnimation(ViewEntries.this, R.anim.fade_in);
                            animation.setDuration(500);
                            list.setAnimation(animation);
                        }
                        else {
                            updatePlot();
                        }

                    }

                    @Override
                    public void onEntryRefreshFail() {

                    }
                });
                daTask.execute(weekEndingId);
                weekEndingButton.setText(DateConverter.switchToFridayInString(cal));
                Toast.makeText(ViewEntries.this, "Selected week ending: " + cal.getTime(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNoConnection() {
                Toast.makeText(ViewEntries.this, "No internet connection", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDateUnavailable(View caldroidCell) {
                Animation animation = AnimationUtils.loadAnimation(ViewEntries.this, R.anim.shake);
                animation.setDuration(500);
                caldroidCell.startAnimation(animation);
                Toast.makeText(ViewEntries.this, "Out of range, out of sight, out of mind", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void setRefreshEntries(boolean refreshEntries) {
        ViewEntries.refreshEntries = refreshEntries;
    }

    //TODO plot classes
    class MyBarFormatter extends BarFormatter {
        public MyBarFormatter(int fillColor, int borderColor) {
            super(fillColor, borderColor);
        }

        @Override
        public Class<? extends SeriesRenderer> getRendererClass() {
            return MyBarRenderer.class;
        }

        @Override
        public SeriesRenderer getRendererInstance(XYPlot plot) {
            return new MyBarRenderer(plot);
        }
    }

    class MyBarRenderer extends BarRenderer<MyBarFormatter> {

        public MyBarRenderer(XYPlot plot) {
            super(plot);
        }

        /**
         * Implement this method to change the formatter when a bar is selected
         */
        @Override
        public MyBarFormatter getFormatter(int index, XYSeries series) {
            if (selection != null &&
                    selection.second == series &&
                    selection.first == index) {
                return selectionFormatter;
            } else {
                return getFormatter(series);
            }
        }
    }

    class GraphXLabelFormat extends Format {

        @Override
        public StringBuffer format(Object arg0, StringBuffer arg1, FieldPosition arg2) {

            int parsedInt = Math.round(Float.parseFloat(arg0.toString()));
            Log.d("test", parsedInt + " " + arg1 + " " + arg2);
            String labelString = X_LABELS[parsedInt];
            arg1.append(labelString);
            return arg1;
        }

        @Override
        public Object parseObject(String arg0, ParsePosition arg1) {
            return java.util.Arrays.asList(X_LABELS).indexOf(arg0);
        }
    }
}

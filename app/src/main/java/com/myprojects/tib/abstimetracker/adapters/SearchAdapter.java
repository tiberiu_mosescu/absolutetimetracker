package com.myprojects.tib.abstimetracker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.objects.ListViewRow;
import com.myprojects.tib.abstimetracker.objects.User;

import java.util.ArrayList;

/**
 * Used for the Add Entry search. Only difference from CustomAdapter is charSequence.length()>2 instead of 0.
 * This lets the user input at least 3 characters before the list with results is displayed.
 * It uses the same layout as CustomAdapter R.layout.view_list_row, but if need arises it can be changed.
 * If the layout is changed, don't forget to change ViewHolder and associated methods too.
 */
public class SearchAdapter extends BaseAdapter implements Filterable {
    private LayoutInflater inflater;
    private ArrayList<ListViewRow> rows;

    private class ViewHolder {
        LinearLayout left_iv_LL;
        TextView tv_date;
        TextView tv_description;
        TextView tv_project;
        TextView tv_duration;
    }

    public SearchAdapter (Context context, ArrayList<ListViewRow> rows) {
        inflater = LayoutInflater.from(context);
        this.rows = rows;
    }
    public ArrayList<ListViewRow> getAdapterList () {
        return rows;
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    public void addAll (ArrayList<ListViewRow> newRows) {
        rows=newRows;
        notifyDataSetChanged();
    }

    public void clear () {
        rows.clear();
        notifyDataSetChanged();
    }

    @Override
    public ListViewRow getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.view_list_row, null);
            holder.left_iv_LL = (LinearLayout) convertView.findViewById(R.id.view_entry_leftLL);
            holder.tv_date = (TextView) convertView.findViewById(R.id.viewEntry_day);
            holder.tv_description = (TextView) convertView.findViewById(R.id.viewEntry_description);
            holder.tv_project = (TextView) convertView.findViewById(R.id.viewEntry_project);
            holder.tv_duration = (TextView) convertView.findViewById(R.id.viewEntry_duration);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position > 0 && rows.get(position).getEntryDate().equals(rows.get(position-1).getEntryDate())) {
            holder.left_iv_LL.setVisibility(View.INVISIBLE);
            holder.tv_date.setVisibility(View.GONE);
        }
        else {
            holder.left_iv_LL.setVisibility(View.VISIBLE);
            holder.tv_date.setVisibility(View.VISIBLE);
            holder.tv_date.setText(rows.get(position).getEntryDate());
        }

        holder.tv_description.setText(rows.get(position).getDescription());
        holder.tv_project.setText(rows.get(position).getProject());
        holder.tv_duration.setText(rows.get(position).getDuration() + " hours   (" + rows.get(position).getStartHour() + " -> " + rows.get(position).getEndHour() + ")" );
        return convertView;
    }
    public Filter getFilter () {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ListViewRow> results = new ArrayList<>();
                if (charSequence != null && charSequence.length()>2) {
                    if (rows!= null && rows.size()>0) {
                        for (final ListViewRow oneRow : rows) {
                            if (oneRow.getProject().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                results.add(oneRow);
                            }
                        }
                    }
                    oReturn.values = results;
                }
                else {
                    rows = User.getUserInstance().getAllRowsList(); //unfiltered list
                    oReturn.values = rows;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                rows = (ArrayList<ListViewRow>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * Sets the ListView height dynamically based on item height
     * @param listView to be resized
     * @return true if succeeded, false otherwise
     */

    public static int getListViewHeightBasedOnItems(ListView listView, SearchAdapter listAdapter) {
        if (listAdapter != null) {
            int numberOfItems = listAdapter.getCount();

            //Get total height of all items
            int totalItemHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemHeight += item.getMeasuredHeight();
            }
            //Get total height of dividers
            int totalDividerHeight = 0;
            if (numberOfItems > 1)
                totalDividerHeight = listView.getDividerHeight() * (numberOfItems-1);


            //Set list height
//            ViewGroup.LayoutParams params = listView.getLayoutParams();
//            params.height = totalItemHeight + totalDividerHeight;
//            listView.setLayoutParams(params);
//            listView.requestLayout();
            return totalItemHeight+totalDividerHeight;
        }
        else {
            return ViewGroup.LayoutParams.MATCH_PARENT;
        }
    }
}

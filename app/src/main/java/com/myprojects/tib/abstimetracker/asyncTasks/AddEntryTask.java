package com.myprojects.tib.abstimetracker.asyncTasks;

import android.app.Activity;
import android.os.AsyncTask;

import com.myprojects.tib.abstimetracker.objects.EditUtilObject;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.JsonBuilders;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

public class AddEntryTask extends AsyncTask<EditUtilObject, Void, Integer> {
    Activity activity;
    AddEntryListener listener;

    public AddEntryTask (Activity activity, AddEntryListener listener) {
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    protected Integer doInBackground(EditUtilObject... addFields) {
        int status;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost(Connection.ADD_ENTRY_URL);
            StringEntity entity = new StringEntity(JsonBuilders.addEntryJson(addFields[0]).toString());
            entity.setContentType("application/json");
            post.setEntity(entity);
            HttpResponse response = httpClient.execute(post);
            status = response.getStatusLine().getStatusCode();
            return status;
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    protected void onPostExecute(Integer status) {
        super.onPostExecute(status);
        if (status == 200) {
            listener.onAddEntrySuccess();
        }
        else {
            listener.onAddEntryFail();
        }
    }

    public interface AddEntryListener {
        void onAddEntrySuccess();
        void onAddEntryFail();
    }
}

package com.myprojects.tib.abstimetracker.asyncTasks;

import android.app.Activity;
import android.os.AsyncTask;

import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.JsonBuilders;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class DeleteTask extends AsyncTask<Long, Void, Integer> {
    Activity activity;
    DeleteEntryListener listener;


    public DeleteTask (Activity activity, DeleteEntryListener listener) {
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    protected Integer doInBackground(Long... longs) {
        int status;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Connection.ENTRY_DELETE_URL + longs[0]); // pass logId as long
            StringEntity tokenEntity = new StringEntity(JsonBuilders.tokenJson().toString());
            tokenEntity.setContentType("application/json");
            httpPost.setEntity(tokenEntity);
            HttpResponse response = httpClient.execute(httpPost);
            status = response.getStatusLine().getStatusCode();
            return status;
        } catch (UnsupportedEncodingException e) {
            return 0;
        } catch (ClientProtocolException e) {
            return 0;
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    protected void onPostExecute(Integer status) {
        super.onPostExecute(status);
        if (status == 200) {
            listener.onDeleteSuccessful();
        }
        else {
            listener.onDeleteFail();
        }
    }

    public interface DeleteEntryListener {
        void onDeleteSuccessful();
        void onDeleteFail();
    }
}

package com.myprojects.tib.abstimetracker.asyncTasks;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.myprojects.tib.abstimetracker.objects.User;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.JsonBuilders;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class RefreshEntriesTask extends AsyncTask<Integer, Void, Integer> {
    private Activity activity;
    private SharedPreferences prefs;
    private RefreshEntriesListener listener;

    public RefreshEntriesTask (Activity activity, SharedPreferences prefs, RefreshEntriesListener listener) {
        this.activity = activity;
        this.prefs = prefs;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onPreRefreshEntries();
    }

    @Override
    protected Integer doInBackground(Integer... weekEndingId) {
        User user = User.getUserInstance();
        try {
            //POST http://50.63.53.33:8083/TimeTrackerService.svc/employees/{userId}/timeEntries/{weekEndingId}
            HttpClient httpClient = new DefaultHttpClient();
            String jsonToken = ((new JSONObject()).put("token", user.getToken())).toString();
            HttpPost timeEntriesPost = new HttpPost(Connection.TIME_TRACKER_TEMPLATE + user.getUserId() + "/timeEntries/" + weekEndingId[0]);
            StringEntity tokenEntity = new StringEntity(jsonToken);
            tokenEntity.setContentType("application/json");
            timeEntriesPost.setEntity(tokenEntity);
            HttpResponse timeEntriesResponse = httpClient.execute(timeEntriesPost);
            user.setAllRowsList(JsonBuilders.buildEntryFromJson(EntityUtils.toString(timeEntriesResponse.getEntity())));
            return timeEntriesResponse.getStatusLine().getStatusCode();

        } catch (JSONException | IOException e) {
            return -1;
        }
    }

    @Override
    protected void onPostExecute(Integer status) {
        super.onPostExecute(status);
            if (status == 200) {
                listener.onEntriesRefreshed();
            }
            else {
                listener.onEntryRefreshFail();
            }
    }

    public interface RefreshEntriesListener {
        void onPreRefreshEntries();
        void onEntriesRefreshed();
        void onEntryRefreshFail();
    }
}

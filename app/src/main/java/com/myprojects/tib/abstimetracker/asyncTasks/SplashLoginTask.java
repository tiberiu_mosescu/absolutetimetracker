package com.myprojects.tib.abstimetracker.asyncTasks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.myprojects.tib.abstimetracker.activities.LoginActivity;
import com.myprojects.tib.abstimetracker.activities.ViewEntries;
import com.myprojects.tib.abstimetracker.objects.User;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.DateConverter;
import com.myprojects.tib.abstimetracker.utils.JsonBuilders;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

public class SplashLoginTask extends AsyncTask<String, Void, User> {
    private static final String TAG = "LoginTask";
    private ProgressBar progressBar;
    private Activity activity;
    private SharedPreferences prefs;

    public SplashLoginTask (Activity activity, ProgressBar progressBar) {
        this.activity = activity;
        this.progressBar = progressBar;
        prefs = activity.getSharedPreferences(User.PREFS_NAME, Context.MODE_PRIVATE);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected User doInBackground(String... credentials) {

        User user = User.getUserInstance();
        String jsonWeekEndings;
        int weekEndingId = 0;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Connection.SIGN_IN_URL);
            StringEntity entity = new StringEntity(JsonBuilders.createSplashJSONcredentials(prefs).toString());
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost);
            int status = response.getStatusLine().getStatusCode();
            String jsonResponse = EntityUtils.toString(response.getEntity());
            JSONObject json = new JSONObject(jsonResponse); //outer JSON {"d":{...}}
            JSONObject json_d = json.getJSONObject("d"); // inner JSON
            user.setToken(json_d.getString("Token"));
            user.setUserId(json_d.getInt("UserId"));
            String jsonToken = ((new JSONObject()).put("token", user.getToken())).toString();

            if (status == 200) {
                //POST http://50.63.53.33:8083/TimeTrackerService.svc/employees/{userId}/projects
                HttpPost projectsPost = new HttpPost(Connection.TIME_TRACKER_TEMPLATE + user.getUserId() + "/projects");
                StringEntity tokenEntity = new StringEntity(jsonToken);
                tokenEntity.setContentType("application/json");
                projectsPost.setEntity(tokenEntity);
                HttpResponse projectsResponse = httpClient.execute(projectsPost);
                user.setProjectArrayList(JsonBuilders.getProjectList(EntityUtils.toString(projectsResponse.getEntity()), prefs));
                //GET http://50.63.53.33:8083/TimeTrackerService.svc/weekEndings
                HttpGet weekEndingsGet = new HttpGet(Connection.WEEK_ENDINGS_URL);
                HttpResponse weekEndingsResponse = httpClient.execute(weekEndingsGet);
                jsonWeekEndings = EntityUtils.toString(weekEndingsResponse.getEntity());
                long now = Calendar.getInstance().getTimeInMillis();
                JSONArray datesArray = new JSONArray(jsonWeekEndings);
                for (int i = 0; i < datesArray.length(); i++) {
                    JSONObject dateJSON = datesArray.getJSONObject(i);
                    String date = dateJSON.getString("WeekEndingDate");
                    int dateId = dateJSON.getInt("WeekEndingID");
                    prefs.edit().putInt(date.substring(date.indexOf("(")+1,
                            date.lastIndexOf("-")), dateId).apply();
                    if (DateConverter.isWeekEnding(date, now)) {
                        weekEndingId = dateJSON.getInt("WeekEndingID");
                        prefs.edit().putInt(User.PREFS_WEEK_ENDING, weekEndingId)
                                .apply();
                        break;
                    }
                }

                //POST http://50.63.53.33:8083/TimeTrackerService.svc/employees/{userId}/timeEntries/{weekEndingId}
                HttpPost timeEntriesPost = new HttpPost(Connection.TIME_TRACKER_TEMPLATE + user.getUserId() + "/timeEntries/" + weekEndingId);
                timeEntriesPost.setEntity(tokenEntity);
                HttpResponse timeEntriesResponse = httpClient.execute(timeEntriesPost);
                user.setAllRowsList(JsonBuilders.getAllRowsList(EntityUtils.toString(timeEntriesResponse.getEntity())));
                //TODO add issues querry
            }

            return user;


        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException happened", e);
            return user;
        } catch (ClientProtocolException e) {
            Log.e(TAG, "ClientProtocolException happened", e);
            return user;
        } catch (IOException e) {
            Log.e(TAG, "IOException happened", e);
            return user;
        } catch (JSONException e) {
            Log.e(TAG, "JSONException happened", e);
            return user;
        }
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        if (user.getToken() != null && user.getUserId() !=0) {
            Intent intent = new Intent(activity, ViewEntries.class);
            activity.startActivity(intent);
            activity.finish();
        }
        else {
            Intent intent = new Intent(activity, LoginActivity.class);
            activity.startActivity(intent);
            activity.finish();
        }

        progressBar.setVisibility(View.INVISIBLE);
    }
}

package com.myprojects.tib.abstimetracker.dialogs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.objects.CaldroidUtilObject;
import com.myprojects.tib.abstimetracker.utils.Connection;
import com.myprojects.tib.abstimetracker.utils.DateConverter;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;

/**
 * Dialog displayed when pressing the button that sets the current date/week in View Entries
 */
public class CaldroidDialog {
    private static final String CALDROID_TAG = "DAT DIALOG CALENDAR";

    public static void showCaldroidDialog (final FragmentActivity activity, final SharedPreferences prefs, final CaldroidDateListener listener) {
        final CaldroidFragment caldroidFragment = CaldroidFragment.newInstance((String)activity.getText(R.string.CALDROID_SELECT_WEEK), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.YEAR));
        caldroidFragment.setBackgroundResourceForDates(DateConverter.getBackgroundForFridays());
        caldroidFragment.setBackgroundResourceForDate(R.drawable.red_border, new Date());
        caldroidFragment.refreshView();
        caldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View caldroidCell) {
                CaldroidUtilObject object = DateConverter.getWeekEndingId(date, prefs);
                if (object.getWeekEndingId() != -1) {
                    if (Connection.isNetworkConnAvailable(activity)) {
                        listener.onDateChosen(date, object.getCalendar(), object.getWeekEndingId(), caldroidCell);
                        caldroidFragment.dismiss();
                    }
                    else {
                        listener.onNoConnection();
                    }
                }
                else {
                    listener.onDateUnavailable(caldroidCell);
                }
            }
        });
        Bundle args = new Bundle();
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.SUNDAY);
        caldroidFragment.setArguments(args);
        caldroidFragment.show(activity.getSupportFragmentManager(), CALDROID_TAG);
    }
    public interface CaldroidDateListener {
        void onDateChosen(Date date, Calendar updatedCalendar, int weekEndingId, View caldroidCell);
        void onNoConnection();
        void onDateUnavailable(View caldroidCell);
    }
}



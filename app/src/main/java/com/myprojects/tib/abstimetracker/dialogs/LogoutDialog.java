package com.myprojects.tib.abstimetracker.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.activities.LoginActivity;
import com.myprojects.tib.abstimetracker.objects.User;

/**
 * Dialog displayed when the logout menu icon from View Entries is pressed
 */
public class LogoutDialog {
    public static void createDialog (final Activity activity, final SharedPreferences prefs) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getText(R.string.APP_MESSAGE_LOGOUT))
                    .setMessage(activity.getText(R.string.APP_MESSAGE_CONFIRM_LOGOUT))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            User.reset();
                            prefs.edit().putString("email", null).apply();
                            Intent intent = new Intent(activity, LoginActivity.class);
                            activity.startActivity(intent);
                            activity.finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
    }
}

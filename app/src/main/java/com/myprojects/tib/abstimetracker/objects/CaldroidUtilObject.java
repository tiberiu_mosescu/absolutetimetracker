package com.myprojects.tib.abstimetracker.objects;

import java.util.Calendar;

/**
 * Used for transporting different types of data
 */
public class CaldroidUtilObject {
    private Calendar calendar;
    private int weekEndingId;

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public int getWeekEndingId() {
        return weekEndingId;
    }

    public void setWeekEndingId(int weekEndingId) {
        this.weekEndingId = weekEndingId;
    }
}

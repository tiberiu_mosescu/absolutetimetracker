package com.myprojects.tib.abstimetracker.objects;

/**
 * Used for Add/Edit AsyncTask as injected parameter for task.execute(EditUtilObject)
 */
public class EditUtilObject {
    private String entryId, duration, descriptionText, categoryId, endHour,
            entryDate, issueId, moduleId, projectId, startHour;

    public EditUtilObject(String entryId, String duration, String descriptionText, String categoryId, String endHour, String entryDate, String issueId, String moduleId, String projectId, String startHour) {
        this.entryId = entryId;
        this.duration = duration;
        this.descriptionText = descriptionText;
        this.categoryId = categoryId;
        this.endHour = endHour;
        this.entryDate = entryDate;
        this.issueId = issueId;
        this.moduleId = moduleId;
        this.projectId = projectId;
        this.startHour = startHour;
    }

    public String getEntryId() {
        return entryId;
    }

    public String getDuration() {
        return duration;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getEndHour() {
        return endHour;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public String getIssueId() {
        return issueId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getStartHour() {
        return startHour;
    }
}

package com.myprojects.tib.abstimetracker.objects;

import java.io.Serializable;

/**
 * The contents of a row in the View Entries activity, but also passed for Editing and other purposes
 */
public class ListViewRow implements Serializable{
    // The row for ViewEntries
    private long entryLogId;
    private String entryDate;
    private String description;
    private String project;
    private String duration;
    private String category;
    private String startHour;
    private String endHour;


    public ListViewRow(String entryDate, String description, String project, String duration, String category, String startHour, String endHour, long entryLogId) {
        this.entryDate = entryDate;
        this.description = description;
        this.project = project;
        this.duration = duration;
        this.category = category;
        this.startHour = startHour;
        this.endHour = endHour;
        this.entryLogId = entryLogId;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public String getDescription() {
        return description;
    }

    public String getProject() {
        return project;
    }

    public String getDuration() {
        return duration;
    }

    public String getCategory() {
        return category;
    }

    public String getStartHour() {
        return startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public long getEntryLogId() {
        return entryLogId;
    }


}

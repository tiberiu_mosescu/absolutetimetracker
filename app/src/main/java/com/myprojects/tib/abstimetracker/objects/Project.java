package com.myprojects.tib.abstimetracker.objects;

import java.util.ArrayList;

/**
 * Project with its assigned categories for Add Entry spinners
 */
public class Project {
    private String name; //Absolute - General ...
    private ArrayList<String> categories; //DEV, DBUG ...
    private ArrayList<Integer> categoryId;
    private int projectId;


    public Project(String name, ArrayList<String> categories, ArrayList<Integer> categoryId, int projectId) {
        this.name = name;
        this.categories = categories;
        this.categoryId = categoryId;
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public ArrayList<Integer> getCategoryId() {
        return categoryId;
    }

    public int getProjectId() {
        return projectId;
    }
}

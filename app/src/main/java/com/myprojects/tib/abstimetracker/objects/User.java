package com.myprojects.tib.abstimetracker.objects;

import java.util.ArrayList;

/**
 * Global singleton that holds the user info and data. Called throughout the whole app.
 */
public class User {
    public static final String PREFS_NAME = "Prefs";
    public static final String PREFS_WEEK_ENDING = "currentWeekEnding";

    private static User instance = null;

    private String userName;
    private String password;
    private int userId;
    private String token;

    private ArrayList<ListViewRow> allRowsList;
    private ArrayList<Project> projectArrayList;

    private User() {
    }

    public static User getUserInstance () {
        if (instance == null) {
            instance = new User();
        }
        return instance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<ListViewRow> getAllRowsList() {
        return allRowsList;
    }

    public void setAllRowsList(ArrayList<ListViewRow> allRowsList) {
        this.allRowsList = allRowsList;
    }

    public ArrayList<Project> getProjectArrayList() {
        return projectArrayList;
    }

    public void setProjectArrayList(ArrayList<Project> projectArrayList) {
        this.projectArrayList = projectArrayList;
    }

    public static void reset () {
        instance = null;
    }
}

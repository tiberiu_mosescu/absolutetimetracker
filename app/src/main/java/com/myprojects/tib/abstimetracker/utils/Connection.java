package com.myprojects.tib.abstimetracker.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Class that holds the URLs and checks if an internet connection is available
 */
public class Connection {
    public static final String LOCALHOST = "http://50.63.53.33:8083/";
    public static final String SIGN_IN_URL = LOCALHOST + "AuthenticationService.svc/SignIn";
    public static final String TIME_TRACKER_TEMPLATE = LOCALHOST + "TimeTrackerService.svc/employees/";
    public static final String WEEK_ENDINGS_URL = LOCALHOST + "TimeTrackerService.svc/weekEndings";
    public static final String ADD_ENTRY_URL = LOCALHOST + "TimeTrackerService.svc/timeEntries/add";
    public static final String ENTRY_DELETE_URL = LOCALHOST + "TimeTrackerService.svc/timeEntries/delete/";

    public static boolean isNetworkConnAvailable(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

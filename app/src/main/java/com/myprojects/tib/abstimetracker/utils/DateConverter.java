package com.myprojects.tib.abstimetracker.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.myprojects.tib.abstimetracker.R;
import com.myprojects.tib.abstimetracker.objects.CaldroidUtilObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Everything in the app related to date parsing, Calendars, date display and others
 */
public class DateConverter {
    //Constants
    private static final long MILLIS_IN_DAY = 86400000;
    private static final long MILLIS_IN_WEEK = 604800000;

    public static String longToDate (String longToConvert) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy", Locale.ENGLISH);
        String millisCut = longToConvert.substring(longToConvert.indexOf("(")+1,
                longToConvert.lastIndexOf("-"));
        long millis = Long.parseLong(millisCut);
        return sdf.format(new Date(millis));
    }

    /**
    Converts calendar to (current date) 12:00:00 AM GMT in milliseconds.
    Example Thursday, March 5, 2015 12:00:00 AM GMT is 1,425,513,600,000 milliseconds
     */
    public static String convertToGmt(Calendar cal) {
        // Converts calendar to (current date) 12:00:00 AM GMT in milliseconds
        //Example

        TimeZone gmtTime = TimeZone.getTimeZone("GMT");
        cal.setTimeZone(gmtTime);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis() + "";
    }

    public static Date convertStringToDate (String dateInString, Context context) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a", Locale.ENGLISH);
        SimpleDateFormat sdf2 = new SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.ENGLISH);
        Date date;
        try {
            date = sdf.parse(dateInString);
        } catch (ParseException e) {
            try {
                date = sdf2.parse(dateInString);
            } catch (ParseException e1) {
                date = new Date();
                Toast.makeText(context, "Warning: Date in unknown format, current date is shown", Toast.LENGTH_LONG).show();
                e1.printStackTrace();
            }
        }
        return date;
    }
        public static String switchToFridayInString (Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        TimeZone gmtTime = TimeZone.getTimeZone("GMT");
        cal.setTimeZone(gmtTime);
        cal.setFirstDayOfWeek(Calendar.SUNDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        return "Entries for week " + sdf.format(cal.getTime()) + "\n(tap to change week)";
    }

    public static boolean isWeekEnding (String longDate, long now) {
        String millis = longDate.substring(longDate.indexOf("(")+1, longDate.lastIndexOf("-"));
        long longMillis = Long.parseLong(millis);
        longMillis += MILLIS_IN_DAY;
        return longMillis - now > 0 && longMillis - now < MILLIS_IN_WEEK;
    }

    public static HashMap<Date, Integer> getBackgroundForFridays () {
        HashMap<Date, Integer> backgroundForFridays = new HashMap<>();
        Calendar today = Calendar.getInstance();
        today.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        Calendar threeMonthsAgo = Calendar.getInstance();
        threeMonthsAgo.add(Calendar.WEEK_OF_YEAR, -9);
        while (threeMonthsAgo.before(today)) {
            if (threeMonthsAgo.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
                backgroundForFridays.put(threeMonthsAgo.getTime(), R.drawable.caldroid_friday_arrow);
                threeMonthsAgo.add(Calendar.WEEK_OF_YEAR, 1);
            }
            else {
                threeMonthsAgo.add(Calendar.DAY_OF_YEAR, 1);
            }
        }
        backgroundForFridays.put(Calendar.getInstance().getTime(), R.color.lightOrange);
        return backgroundForFridays;
    }

    /**
     Checks to see if the week selected was between the ones returned by the server
     and returns its ID if so. Else it returns -1.
     */
    public static CaldroidUtilObject getWeekEndingId (Date date, SharedPreferences prefs) {
        CaldroidUtilObject object = new CaldroidUtilObject();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        TimeZone gmtTime = TimeZone.getTimeZone("GMT");
        cal.setTimeZone(gmtTime);
        cal.setFirstDayOfWeek(Calendar.SUNDAY);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);

        int weekEndingId = -1;
        for (int i=0; i<24; i++) {
            if (prefs.getInt(cal.getTimeInMillis()+"", -1) == -1) {
                cal.add(Calendar.HOUR_OF_DAY, 1);
            }
            else {
                weekEndingId = prefs.getInt(cal.getTimeInMillis()+"", -1);
                break;
            }
        }
        object.setCalendar(cal);
        object.setWeekEndingId(weekEndingId);
        return object;
    }
}

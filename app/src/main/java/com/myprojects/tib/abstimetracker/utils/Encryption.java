package com.myprojects.tib.abstimetracker.utils;

import android.util.Base64;

/**
 * Class that encodes the user password into Base64. Security can be improved.
 */
public class Encryption {

    public static String toBase64 (String input) {
        return Base64.encodeToString(input.getBytes(), Base64.NO_WRAP);
    }
}

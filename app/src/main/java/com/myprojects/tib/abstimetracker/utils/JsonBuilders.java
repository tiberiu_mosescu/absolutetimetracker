package com.myprojects.tib.abstimetracker.utils;

import android.content.SharedPreferences;

import com.myprojects.tib.abstimetracker.objects.EditUtilObject;
import com.myprojects.tib.abstimetracker.objects.ListViewRow;
import com.myprojects.tib.abstimetracker.objects.Project;
import com.myprojects.tib.abstimetracker.objects.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Everything related to either building JSONs or decomposing JSONs and building objects from them
 */

public class JsonBuilders {
    public static JSONObject createJSONcredentials (String username, String password) {
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("password", Encryption.toBase64(password));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONObject createSplashJSONcredentials (SharedPreferences prefs) {
        JSONObject json = new JSONObject();
        try {
            json.put("username", prefs.getString("email", null));
            json.put("password", prefs.getString("password64", null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static JSONObject addEntryJson (EditUtilObject editUtilObject) {
        User user = User.getUserInstance();
        JSONObject object = new JSONObject();
        JSONObject entryLogJSON = new JSONObject();
        try {
            entryLogJSON.put("Description", editUtilObject.getDescriptionText());
            entryLogJSON.put("Duration", Double.parseDouble(editUtilObject.getDuration()));
            entryLogJSON.put("CategoryID", editUtilObject.getCategoryId());
            entryLogJSON.put("EndHour", editUtilObject.getEndHour());
            entryLogJSON.put("EntryDate", editUtilObject.getEntryDate());
            entryLogJSON.put("IssueID", editUtilObject.getIssueId());
            entryLogJSON.put("ModuleID", editUtilObject.getModuleId());
            entryLogJSON.put("ProjectID", editUtilObject.getProjectId());
            entryLogJSON.put("StartHour", editUtilObject.getStartHour());
            entryLogJSON.put("UserID", user.getUserId());
            object.put("token", user.getToken());
            object.put("entryLog", entryLogJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static JSONObject editEntryJson (EditUtilObject editUtilObject) {
        User user = User.getUserInstance();
        JSONObject object = new JSONObject();
        JSONObject entryLogJSON = new JSONObject();
        try {
            entryLogJSON.put("EntryId", Long.parseLong(editUtilObject.getEntryId()));
            entryLogJSON.put("Description", editUtilObject.getDescriptionText());
            entryLogJSON.put("Duration", Double.parseDouble(editUtilObject.getDuration()));
            entryLogJSON.put("CategoryID", editUtilObject.getCategoryId());
            entryLogJSON.put("EndHour", editUtilObject.getEndHour());
            entryLogJSON.put("EntryDate", editUtilObject.getEntryDate());
            entryLogJSON.put("IssueID", editUtilObject.getIssueId());
            entryLogJSON.put("ModuleID", editUtilObject.getModuleId());
            entryLogJSON.put("ProjectID", editUtilObject.getProjectId());
            entryLogJSON.put("StartHour", editUtilObject.getStartHour());
            entryLogJSON.put("UserID", user.getUserId());
            object.put("token", user.getToken());
            object.put("entryLog", entryLogJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static JSONObject tokenJson () {
        User user = User.getUserInstance();
        JSONObject json = new JSONObject();
        String token = user.getToken();
        try {
            json.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static ArrayList<ListViewRow> buildEntryFromJson (String timeEntries) {
        ListViewRow object;
        ArrayList<ListViewRow> list = new ArrayList<>();
        try {
            JSONArray entriesArray = new JSONArray(timeEntries);
            for (int i=0; i<entriesArray.length(); i++) {
                JSONObject entryJSON = entriesArray.getJSONObject(i);
                String date = DateConverter.longToDate(entryJSON.getString("EntryDate"));
                String description = entryJSON.getString("Description");
                String project = entryJSON.getString("ProjectName");
                String duration = entryJSON.getString("Duration");
                String category = entryJSON.getString("CategoryName");
                String startHour = entryJSON.getString("StartHour");
                String endHour = entryJSON.getString("EndHour");
                long entryLogId = entryJSON.getLong("EntryLogID");
                object = new ListViewRow(date, description, project, duration, category, startHour, endHour, entryLogId);
                list.add(object);
            }
        } catch (JSONException e) {
            return null;
        }
        return list;
    }

    public static ArrayList<ListViewRow> getAllRowsList (String jsonTimeEntries) {
        ArrayList<ListViewRow> allRowsList = new ArrayList<>();
        // Parse JSONs and retrieve data

        //Time entries for ViewEntries activity
        try {
            JSONArray entriesArray = new JSONArray(jsonTimeEntries);
            for (int i=0; i<entriesArray.length(); i++) {
                JSONObject entryJSON = entriesArray.getJSONObject(i);
                String date = DateConverter.longToDate(entryJSON.getString("EntryDate"));
                String description = entryJSON.getString("Description");
                String project = entryJSON.getString("ProjectName");
                String duration = entryJSON.getString("Duration");
                String category = entryJSON.getString("CategoryName");
                String startHour = entryJSON.getString("StartHour");
                String endHour = entryJSON.getString("EndHour");
                long entryLogId = entryJSON.getLong("EntryLogID");
                ListViewRow object = new ListViewRow(date, description, project, duration, category, startHour, endHour, entryLogId);
                allRowsList.add(object);
            }
        } catch (JSONException e) {
            allRowsList = new ArrayList<>();
        }
        return allRowsList;
    }

    public static ArrayList<Project> getProjectList (String jsonProjects, SharedPreferences prefs) {
        //Projects for Spinners in AddEntry activity
        ArrayList<Project> projectsList = new ArrayList<>();
        try {
            JSONArray projectsArray = new JSONArray(jsonProjects);
            for (int i=0; i<projectsArray.length(); i++) {
                int status;
                JSONObject projectJSON = projectsArray.getJSONObject(i);
                try {
                    status = projectJSON.getInt("Status");
                }
                catch (JSONException e) {
                    status = 0;
                }
                if (status == 1) {
                    String name = projectJSON.getString("Name");
                    JSONArray categoriesArray = projectJSON.getJSONArray("ProjectCategories");
                    ArrayList<String> categoryList = new ArrayList<>();
                    ArrayList<Integer> categoryIds = new ArrayList<>();
                    int projectId = 0;
                    for (int j = 0; j < categoriesArray.length(); j++) {
                        JSONObject categoyJSON = categoriesArray.getJSONObject(j);
                        categoryList.add(categoyJSON.getString("Name")); // ADMN
                        prefs.edit().putString(categoyJSON.getString("Abbreviation"), categoyJSON.getString("Name")).apply();
                        categoryIds.add(categoyJSON.getInt("CategoryID"));
                        projectId = categoyJSON.getInt("ProjectID");
                    }
                        projectsList.add(new Project(name, categoryList, categoryIds, projectId));
                }
            }
        } catch (JSONException e) {
            projectsList = new ArrayList<>();
        }
        return projectsList;
    }
}
